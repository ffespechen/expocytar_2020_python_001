#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 13:49:40 2020

@author: ffespechen
"""

import pygame , sys
from pygame.locals import *

ancho = 820
alto = 640

def main():
    
    # Instanciar y establecer tamaño de la ventana 
    pantalla = pygame.display.set_mode((ancho, alto))
    # Establecer el nombre de la ventana
    pygame.display.set_caption('Primera ventana')
    
    corriendo = True
    
    while corriendo:
        
        # Bucle de eventos
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                corriendo = False
                
        pygame.display.update()
        
    # Finalizar
    pygame.quit()
    return 0

if __name__ == '__main__':
    pygame.init()
    main()
    
            