#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 15:15:43 2020

@author: ffespechen
"""

import pygame , sys
from pygame.locals import *

import random


ancho = 820
alto = 640

m_ancho = ancho//2
m_alto = alto//2

def main():
    
    # Instanciar y establecer tamaño de la ventana 
    pantalla = pygame.display.set_mode((ancho, alto))
    # Establecer el nombre de la ventana
    pygame.display.set_caption('Demo Dibujos')
    
    
    pygame.draw.line(pantalla, (255,255,255), (m_ancho, 0), (m_ancho, alto), 2)
    pygame.draw.line(pantalla, (255,255,255), (0, m_alto), (ancho, m_alto), 2)
    
    # Círculos concéntricos en el segundo cuadrante
    for radio in range(5, m_alto//2, 5):
        r = random.randint(0,255)
        g = random.randint(0,255)
        b = random.randint(0,255)
        pygame.draw.circle(pantalla, (r, g, b), (m_ancho//2, m_alto//2), radio, 2)
        
    # Lineas diagonales en el cuarto cuadrante
    y_l2 = alto
    for y_l in range(m_alto, alto):
        r = random.randint(0,255)
        g = random.randint(0,255)
        b = random.randint(0,255)
              
        pygame.draw.line(pantalla, (r, g, b), (m_ancho, y_l), (ancho, y_l2), 3)
        y_l2 -= 1
    
    
    corriendo = True

    while corriendo:
        
        # Bucle de eventos
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                corriendo = False
                
        pygame.display.update()
        
    # Finalizar
    pygame.quit()
    return 0

if __name__ == '__main__':
    pygame.init()
    main()
    
