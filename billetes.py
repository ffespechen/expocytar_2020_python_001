#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Imágenes https://www.bcra.gob.ar/MediosPago/Emisiones_vigentes.asp
# Editadas con GIMP https://www.gimp.org/
# Aplicación didáctica para aprender sobre los billetes y monedas vigentes
import pygame, sys
from pygame.locals import *
import random


ancho = 820
alto = 640

pos_X_ini = 10
pos_Y_ini = 5

pos_X_mon = 10
separa_Y_mon = 10

separa_X = 50
separa_Y = 65


color_bg = (0,0,255)

#-- Imágenes de las monedas en circulación
moneda_1 = pygame.image.load('imagenes/peso_1.png')
moneda_2 = pygame.image.load('imagenes/peso_2.png')
moneda_5 = pygame.image.load('imagenes/peso_5.png')
moneda_10 = pygame.image.load('imagenes/peso_10.png')

# Arrays para relacionar imagen y valor de las monedas
img_monedas = [moneda_10, moneda_5, moneda_2, moneda_1]
val_monedas = [10, 5, 2, 1]


#-- Imágenes de los billetes en circulación
billete_20 = pygame.image.load('imagenes/billete_20.gif')
billete_50 = pygame.image.load('imagenes/billete_50.gif')
billete_100 = pygame.image.load('imagenes/billete_100.jpg')
billete_200 = pygame.image.load('imagenes/billete_200.jpg')
billete_500 = pygame.image.load('imagenes/billete_500.jpg')
billete_1000 = pygame.image.load('imagenes/billete_1000.jpg')

# Arrays para relacionar imagen y valor de los billetes
img_billetes = [billete_1000, billete_500, billete_200, billete_100, billete_50, billete_20]
val_billetes = [1000, 500, 200, 100, 50, 20]


reloj = pygame.time.Clock()
random.seed()


def main():

    #-- Inicializando la pantalla
    pantalla = pygame.display.set_mode((ancho, alto))
    pygame.display.set_caption('Juego con billetes y monedas')
    pantalla.fill(color_bg)

    # Arrays para generar cantidades aleatorias de billetes y monedas
    cant_billetes = []
    cant_monedas = []
    
    monto_billetes = 0
    monto_monedas = 0
    
    for i in range(len(img_billetes)):
        cant_billetes.append(random.randint(0,9))
    
    for j in range(len(img_monedas)):
        cant_monedas.append(random.randint(0,9))
    
    reloj.tick(27)
    
    #-- Procesamiento de los montos en billetes
    print('Calculando el monto en billetes')
       
    for indice in range(len(img_billetes)):
        
        monto_billetes += cant_billetes[indice]*val_billetes[indice]
        print(val_billetes[indice], " * ", cant_billetes[indice], " = ", cant_billetes[indice]*val_billetes[indice])
        if(cant_billetes[indice]!=0):
            for v in range(0,cant_billetes[indice]):
                pantalla.blit(img_billetes[indice], (pos_X_ini+v*separa_X, pos_Y_ini+indice*separa_Y))
    
    
    pos_Y_mon = pos_Y_ini+6*separa_Y
    
    #-- Procesamiento de los montos en monedas
    print('Calculando el monto en monedas')
    
    for indice in range(len(img_monedas)):
        
        monto_monedas += cant_monedas[indice]*val_monedas[indice]
        print(val_monedas[indice], " * ", cant_monedas[indice], " = ", cant_monedas[indice]*val_monedas[indice])
        
        if(cant_monedas[indice]!=0):
            for v in range(0, cant_monedas[indice]):
                pantalla.blit(img_monedas[indice], (pos_X_mon+indice*150, pos_Y_mon+v*15))
    
    print('--- Monto Total del Ejercicio ---')
    print(monto_monedas+monto_billetes)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                
        
        pygame.display.update()
        
     
    pygame.quit()
    return 0


if __name__ == '__main__':
    pygame.init()
    main()


