#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 14:26:18 2020

@author: ffespechen
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 13:49:40 2020

@author: ffespechen
"""

import pygame , sys
from pygame.locals import *

ancho = 410
alto = 320

def main():
    
    # Instanciar y establecer tamaño de la ventana 
    pantalla = pygame.display.set_mode((ancho, alto))
    # Establecer el nombre de la ventana
    pygame.display.set_caption('Dibujando líneas')
    
    corriendo = True
    
    # pygame.draw.line -> dibuja un rectángulo
    # Parámetros:
    # Pantalla donde se dibuja
    # Color del la línea
    # Coordenadas inicio del segmento
    # Coordenadas de fin del segmento
    # Ancho de la línea    
    
    # Diagonal Izquierda arriba - Derecha abajo
    pygame.draw.line(pantalla, (255,0,0), (0,0), (ancho, alto), 3)
    
    # Diagonal Derecha arriba - Izquierda abajo
    pygame.draw.line(pantalla, (0,255,0), (ancho, 0), (0, alto), 3)

    
    while corriendo:
        
        # Bucle de eventos
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                corriendo = False
                
        pygame.display.update()
        
    # Finalizar
    pygame.quit()
    return 0

if __name__ == '__main__':
    pygame.init()
    main()
    
            