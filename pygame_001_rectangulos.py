#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 13:49:40 2020

@author: ffespechen
"""

import pygame , sys
from pygame.locals import *

ancho = 410
alto = 320

def main():
    
    # Instanciar y establecer tamaño de la ventana 
    pantalla = pygame.display.set_mode((ancho, alto))
    # Establecer el nombre de la ventana
    pygame.display.set_caption('Dibujando rectángulos')
    
    corriendo = True
    
    # pygame.draw.rect -> dibuja un rectángulo
    # Parámetros:
    # Pantalla donde se dibuja
    # Color de las líneas del rectángulo
    # Coordenadas del punto de la esquina superior izquierda, ancho y alto
    # Ancho del borde
    pygame.draw.rect(pantalla, 
                     (255,255,255), 
                     (0,0,100,150), 
                     1)
    
    pygame.draw.rect(pantalla, (255,255,0), (20,20,100,150), 0)
    pygame.draw.rect(pantalla, (255,0,0), (40,40,100,150), 1)
    pygame.draw.rect(pantalla, (0,255,255), (60,60,100,150), 0)
    pygame.draw.rect(pantalla, (0,255,0), (80,80,100,150), 1)
    
    while corriendo:
        
        # Bucle de eventos
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                corriendo = False
                
        pygame.display.update()
        
    # Finalizar
    pygame.quit()
    return 0

if __name__ == '__main__':
    pygame.init()
    main()
    
            