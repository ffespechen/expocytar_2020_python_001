#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 13:49:40 2020

@author: ffespechen
"""

import pygame , sys
from pygame.locals import *

ancho = 410
alto = 320

def main():
    
    # Instanciar y establecer tamaño de la ventana 
    pantalla = pygame.display.set_mode((ancho, alto))
    # Establecer el nombre de la ventana
    pygame.display.set_caption('Dibujando círculos')
    
    corriendo = True

    # Coordenadas del centro del círculo
    cx = int(ancho/2)
    cy = int(alto/2)
    centro = (cx, cy)
    
    # pygame.draw.circle -> dibuja un rectángulo
    # Parámetros:
    # Pantalla donde se dibuja
    # Color del círculo
    # Coordenadas del centro del círculo
    # radio
    # Ancho del borde, si width=0 se dibuja relleno    
    
    pygame.draw.circle(pantalla, 
                       (255,0,0), 
                       centro, 150, 
                        2)
    
    pygame.draw.circle(pantalla, (128,255,128), centro, 100, 2 )
    pygame.draw.circle(pantalla, (128,128,255), centro, 50)
    pygame.draw.circle(pantalla, (128,255,255), centro, 20, 5)
    pygame.draw.circle(pantalla, (255,255,255), centro, 10)
    
    while corriendo:
        
        # Bucle de eventos
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                corriendo = False
                
        pygame.display.update()
        
    # Finalizar
    pygame.quit()
    return 0

if __name__ == '__main__':
    pygame.init()
    main()
    
            