#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 19:34:28 2020

@author: ffespechen
"""

import math

l1 = [1, 8, 9, 2, 8, 4, 3 ,9, 2, 7, 6]

print('Máximo : ', max(l1))
print('Mínimo : ', min(l1))
print('Promedio : ', sum(l1)/len(l1))

# Funciones del módulo math
for elem in l1:
    print('-'*50)
    print('Elemento ', elem, ' log = ', math.log10(elem))
    print('Elemento ', elem, ' Factorial = ', math.factorial(elem))
    print('Potencia de ', elem, ' elevado a la ', elem, ' es igual a ', math.pow(elem, elem))

# Funciones trigonométricas
lg = [0, 15, 30, 45, 90, 105, 120, 135, 150, 165, 180]

for grad in lg:
    radianes = math.radians(grad)
    print(grad, '° en radianes ', radianes )
    print('sen: ', round(math.sin(radianes), 4), 
          ' cos: ', round(math.cos(radianes), 4),
          ' tg: ', round(math.tan(radianes), 4))

